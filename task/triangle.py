def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    if a > 0 and b > 0 and c > 0:
        if (a + b > c) and (a + c > b) and (b + c > a):
            return True
        else:
            return False
    return False